
-- SUMMARY --

The domain content types module is add on for <a href="http://www.drupal.org/domain">Domain Access</a> which allows you restrict access to content type creation forms based on active domain.

e.g. I want users to be able to create pages on domain x and y, but not on domain z

For a full description of the module, visit the project page:
  http://drupal.org/project/domain_content_types

To submit bug reports and feature suggestions, or to track changes:
  http://drupal.org/project/issues/domain_content_types


-- REQUIREMENTS --

Domain Access Module (http://drupal.org/project/domain)

-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.

-- CONFIGURATION --

* Once the module is installed navigate to the domain list (admin/build/domain/view) and click on the "Content Types" link for each domain to select the content types you wish to use on that domain.

-- CONTACT --

Current maintainers:
* Daniel F. Kudwien (sun) - http://drupal.org/user/54136
* Peter Wolanin (pwolanin) - http://drupal.org/user/49851
* Stefan M. Kudwien (smk-ka) - http://drupal.org/user/48898
* Dave Reid (Dave Reid) - http://drupal.org/user/53892

